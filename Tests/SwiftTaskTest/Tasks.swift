import Foundation
@testable import SwiftTask

class TestTask: Task {
  override func execute() {
    finish(with: nil)
  }
}
class TaskCancelled: Task {

  var msg: String?

  init(msg: String? = nil) {
    self.msg = msg
    super.init()
  }

  override func execute() {
    guard let msg = msg else {
      cancel()
      return
    }

    cancel(with: TaskError(msg))
  }
}

class TaskErrorTest: Task {
  let msg: String

  init(msg: String) {
    self.msg = msg
  }

  override func execute() {
    finish(with: TaskError(msg))
  }
}
