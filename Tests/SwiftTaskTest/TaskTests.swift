import XCTest
@testable import SwiftTask

class TaskTests: XCTestCase {

  var queue: TaskQueue!

  override func setUp() {
    super.setUp()
    // Put setup code here. This method is called before the invocation of each test method in the class.

    queue = TaskQueue()
  }

  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }

  func testTaskStartFinishObservers() {
    let exp = XCTestExpectation(description: "task execution")
    var started = false, finished = false
    let task = TestTask()

    let observer = BlockObserver(start: { _ in
      started = true
    }, finish:  { _, _ in
        finished = true
        exp.fulfill()
    })
    task.add(observer)
    queue.addOperations([task], waitUntilFinished: false)

    wait(for: [exp], timeout: 2.0)
    XCTAssert(started)
    XCTAssert(finished)
  }

  func testTaskCancelObserver() {
    let t1 = TaskCancelled(msg: "Error")
    let t2 = TestTask()
    let exp = XCTestExpectation(description: "task execution 3")
    t2.addDependency(t1)
    queue.add(tasks: [t1, t2])

    t2.add(BlockObserver(cancel: { _, error in
      exp.fulfill()
      XCTAssert(error?.localizedDescription == "Error")
    }))

    wait(for: [exp], timeout: 1.0)
  }

  func testBlockTaskOnMainThread() {
    let exp = XCTestExpectation(description: "BlockTask.mainThread")
    let block = {
      XCTAssert(Thread.current.isMainThread)
      exp.fulfill()
    }
    let t = BlockTask(mainQueue: block)
    queue.add(task: t)
    wait(for: [exp], timeout: 1.0)
  }
}
