import XCTest
@testable import SwiftTask

class TaskErrorTests: XCTestCase {

  func testInitFromOtherTaskError() {
    let er1 = TaskError("Error 1")
    let error = TaskError(er1, and: "Error 2")

    XCTAssert(error.localizedDescription == "Error 1")
    XCTAssert(error.messages == ["Error 1", "Error 2"])
  }

  func testAddMessages() {
    let expected = ["Err 1", "Err 2", "Error 3", "Error 4"]
    let er1 = TaskError(expected[0])
    let er2 = TaskError(Array(expected[1...2]))
    let er3 = er1.add(er2.messages)
    let er4 = er3.add("Error 4")

    print(er4)
    XCTAssert(er4.messages == expected)
  }

}
