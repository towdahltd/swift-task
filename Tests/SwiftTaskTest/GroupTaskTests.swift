import XCTest
@testable import SwiftTask

class GroupTaskTests: XCTestCase {

  var queue: TaskQueue!

  override func setUp() {
    super.setUp()

    queue = TaskQueue()
  }
  override func tearDown() {
    super.tearDown()
    queue = nil
  }

  func testGroupTaskSuccess() {
    let expected = [1, 2, 3]
    var result = [Int]()
    let t1 = TestTask()
    let t2 = TestTask()
    let exp = XCTestExpectation(description: "test 1")
    t1.add(BlockObserver(start: { _ in result.append(1) }))
    t2.add(BlockObserver(start: { _ in result.append(2) }))

    t2.addDependency(t1)

    let group = GroupTask(ops: t1, t2)

    group.add(BlockObserver(finish: { _, _ in
      result.append(3)
      exp.fulfill()
    }))

    queue.add(task: group)

    wait(for: [exp], timeout: 1.0)
    XCTAssert(expected == result)
  }

  func testSubtasksCancellation() {
    let exp = XCTestExpectation(description: "Test 4")
    let t1 = TaskCancelled()
    let t2 = TestTask()
    var started = false
    var result = [String]()
    let expected = ["Start Group", "Start t1", "Finish t1", "Finish t2", "Finish Group"]
    t2.addDependency(t1)
    t1.add(BlockObserver(
      start: { _ in result.append("Start t1") },
      finish: { _, _ in result.append("Finish t1") }))
    t2.add(BlockObserver(
      start: { _ in
        result.append("Start t2")
        started = true
    },
      finish: { _, _ in
        result.append("Finish t2")
    }))

    let group = GroupTask(ops: t1, t2)
    group.add(BlockObserver(
      start: { _ in
        result.append("Start Group")
    },
      finish: { _, _ in
//        print("FINISH GROUP OPERATION --------")
        result.append("Finish Group")
        exp.fulfill()
    }))

    queue.add(task: group)

    wait(for: [exp], timeout: 1.0)
    //    print("RESULT --> \(result)")
    //    print("OPERATIONS COUNT: \(group.internalQueue.operations.count)")

    XCTContext.runActivity(named: "Tasks that depends on cancelled tasks should't start.") { _ in
      XCTAssert(started == false)
    }
    XCTContext.runActivity(named: "Group task should be cancelled too") { _ in
      XCTAssert(group.isCancelled)
      XCTAssert(group.error == TaskError.cancelError)
    }
    XCTContext.runActivity(named: "Check order of tasks execution") { _ in
      XCTAssert(expected == result)
    }
  }

  func testGroupTaskCancelledError() {
    let exp = XCTestExpectation(description: "Test 2")
    let t1 = TaskCancelled(msg: "Err 1")
    let t2 = TaskCancelled(msg: "Err 2")
    var result = [String]()

    t2.addDependency(t1)
    let group = GroupTask(ops: [t2, t1])

    group.add(BlockObserver(finish: { _, error in
      result = error?.messages ?? []
      exp.fulfill()
    }))

    queue.add(task: group)

    wait(for: [exp], timeout: 1.0)
//    print("RESULT: ->> \(result)")
    XCTAssert(result == ["Err 1"])
  }

  func testSubtasksFinishedWithErrors() {
    let exp = XCTestExpectation(description: "Test 3")
    let expected = ["Err 1", "Err 2"]
    let t1 = TaskErrorTest(msg: expected[0])
    let t2 = TaskErrorTest(msg: expected[1])
    var result = [String]()
    t2.addDependency(t1)

    let group = GroupTask(ops: [t2, t1])
    group.add(BlockObserver(finish: { _, error in
      result = error?.messages ?? []
      exp.fulfill()
    }))

    queue.add(task: group)

    wait(for: [exp], timeout: 1.0)
    XCTAssert(expected == result)
  }
}

