import Foundation
import Combine

open class GroupTask: Task {
  // MARK: - Properties
  fileprivate let internalQueue = TaskQueue()
  fileprivate var markedForCancel = false
  fileprivate var combinedError: TaskError?

  fileprivate(set) lazy var finishTask = BlockTask { [weak self] in
    DispatchQueue.main.sync { self?.complete?() }
  }

  // MARK: - Initialization
  public required convenience init(ops: Task...) {
    self.init(ops: ops)
  }
  public init(ops: [Task]) {
    super.init()

    internalQueue.isSuspended = true
    internalQueue.delegate = self

    ops.forEach(add)
  }

  @discardableResult
  public func addToQueue(_ queue: TaskQueue) -> AnyPublisher<Void, TaskError> {
    queue.addOperation(self)

    return subject.eraseToAnyPublisher()
  }

  // MARK: - Override
  open override func execute() {
    internalQueue.addOperation(finishTask)
//    print("Operations ------------> \(internalQueue.operations)")
    internalQueue.isSuspended = false
  }

  open override func cancel() {
    internalQueue.cancelAllOperations()
    internalQueue.isSuspended = false
    super.cancel()
  }

  // MARK: - API
  public func add(task: Task) {
    internalQueue.add(task: task)
  }

  open func taskDidFinish(_ task: Task, error: TaskError?) {}

  // MARK: - Observation
  public typealias SuccessClosure = () -> Void
  public typealias FailureClosure = (TaskError) -> Void
  public typealias CompleteClosure = () -> Void

  private(set) var complete: CompleteClosure?
  private(set) var failure: FailureClosure?
  private(set) var success: SuccessClosure?

  private let subject = PassthroughSubject<Void, TaskError>()

  @discardableResult
  public func complete(_ closure: @escaping CompleteClosure) -> Self {
    complete = closure
    return self
  }
  @discardableResult
  public func failure(_ closure: @escaping FailureClosure) -> Self {
    failure = closure
    return self
  }
  @discardableResult
  public func success(_ closure: @escaping SuccessClosure) -> Self {
    success = closure
    return self
  }
}

// MARK: - TaskQueueDelegate
extension GroupTask: TaskQueueDelegate {
  public func queueWillAdd(task: Task) {
    if task !== finishTask {
      finishTask.addDependency(task)
    }
  }

  public func queueDidCancel(task: Task, with error: TaskError?) {
    markedForCancel = true
  }

  public func queueDidFinish(task: Task, with error: TaskError?) {
    log.info("Group Task: \(self) did finish task: \(task) with error: \(String(describing: error?.messages))")
    if task === finishTask {
      internalQueue.isSuspended = true

      if markedForCancel {
        var error: TaskError?
        if let combinedError = combinedError {
          error = TaskError(combinedError.messages.first ?? "No Error Message", code: combinedError.code)
        }
        DispatchQueue.main.sync {
          self.failure?(error ?? TaskError("Missing Error"))
          self.subject.send(completion: .failure(error ?? TaskError("Missing Error")))
          self.complete?()
        }
        cancel(with: error)
      } else {
        DispatchQueue.main.sync {
            self.success?()
            self.subject.send(())
        }
        finish(with: combinedError)
      }
    } else {
      if let error = error {
        if combinedError != nil {
          combinedError = combinedError?.add(error.messages, code: error.code)
        } else {
          combinedError = error
        }
      }

      taskDidFinish(task, error: error)
    }
  }
}
