import Foundation

public protocol TaskQueueDelegate: AnyObject {
  func queueWillAdd(task: Task)
  func queueDidCancel(task: Task, with error: TaskError?)
  func queueDidFinish(task: Task, with error: TaskError?)
}

open class TaskQueue: OperationQueue {
  weak var delegate: TaskQueueDelegate?

  open override func addOperation(_ op: Operation) {
    guard let task = op as? Task else {
      fatalError("use only Task operations.")
    }

    add(task: task)
  }

  open func add(task: Task) {
    let observer = BlockObserver(
      cancel: { [weak self] task, error in
        self?.delegate?.queueDidCancel(task: task, with: error)
      },
      finish: { [weak self] task, error in
        self?.delegate?.queueDidFinish(task: task, with: error)
      }
    )

    task.add(observer)
    self.delegate?.queueWillAdd(task: task)
    super.addOperation(task)
  }

  public func add(tasks: Task...) {
    add(tasks: tasks)
  }

  open func add(tasks: [Task]) {
    tasks.forEach(add)
  }

  open override func addOperations(_ ops: [Operation], waitUntilFinished wait: Bool) {
    guard let tasks = ops as? [Task] else {
      fatalError("use only Task operations.")
    }
    guard !wait else {
      fatalError("Waiting on operations is an anti-pattern.")
    }

    add(tasks: tasks)
  }
}
