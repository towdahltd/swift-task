import Foundation

// swiftlint:disable type_name

/**
 - Important:
  For timestamp format changing override **dateFormat** property.

  To change minLevel - override **minLevel** property.

 # Defaults
 - default minLevel: **.error**
 - default Timestamp format: **HH:mm:ss.SS**

 # Formatting
 - $T - timestamps
 - $C - level color hearts
 - $M - Module name
 - $F - filename
 - $f - method/function name
 - $l - line number
 - $m - message
 */
public final class Log {
  // MARK: Properties
  private let module: String
  private let dateFormatter = DateFormatter()

  public var minLevel: Level = .error
  public var format: String = "$T: $C $M |$F.$f:$l| --> $m"
  public var dateFormat: String! = "HH:mm:ss.SS"

  // MARK: Initialization
  public init(module: String) {
    self.module = module
  }

  // MARK: - Public 💚
  public func verbose(
    _ message: Any,
    _ file: String = #file,
    _ method: String = #function,
    _ line: Int = #line) {

    if readyToPrint(.verbose) {
      printConsole(message: createMesssage(level: .verbose, "\(message)", file, method, line))
    }
  }
  public func debug(
    _ message: Any,
    _ file: String = #file,
    _ method: String = #function,
    _ line: Int = #line) {

    if readyToPrint(.debug) {
      printConsole(message: createMesssage(level: .debug, "\(message)", file, method, line))
    }
  }
  public func info(
    _ message: Any,
    _ file: String = #file,
    _ method: String = #function,
    _ line: Int = #line) {

    if readyToPrint(.info) {
      printConsole(message: createMesssage(level: .info, "\(message)", file, method, line))
    }
  }
  public func warning(
    _ message: Any,
    _ file: String = #file,
    _ method: String = #function,
    _ line: Int = #line) {

    if readyToPrint(.warning) {
      printConsole(message: createMesssage(level: .warning, "\(message)", file, method, line))
    }
  }
  public func error(
    _ message: Any,
    _ file: String = #file,
    _ method: String = #function,
    _ line: Int = #line) {

    if readyToPrint(.error) {
      printConsole(message: createMesssage(level: .error, "\(message)", file, method, line))
    }
  }
  // MARK: - Internal 🧡
  func readyToPrint(_ level: Level) -> Bool {
    return level >= minLevel
  }
  func createMesssage(
    level: Level,
    _ message: String,
    _ file: String,
    _ method: String,
    _ line: Int) -> String {
    dateFormatter.dateFormat = dateFormat
    var msg = format
    let url = URL(string: file)!
    let fileName = url.deletingPathExtension().lastPathComponent

    msg = msg.replacingOccurrences(of: "$T", with: dateFormatter.string(from: Date()))
    msg = msg.replacingOccurrences(of: "$C", with: level.color)
    msg = msg.replacingOccurrences(of: "$M", with: module)
    msg = msg.replacingOccurrences(of: "$F", with: fileName)
    msg = msg.replacingOccurrences(of: "$f", with: method)
    msg = msg.replacingOccurrences(of: "$l", with: "\(line)")
    msg = msg.replacingOccurrences(of: "$m", with: message)

    return msg
  }

  // MARK: - Private ❤️
  private func printConsole(message: String) {
    DispatchQueue.main.async {
      print(message)
    }
  }
}
