import Foundation

internal enum State: Int, Comparable {

  static func < (lhs: State, rhs: State) -> Bool {
    return lhs.rawValue < rhs.rawValue
  }

  static func == (lhs: State, rhs: State) -> Bool {
    return lhs.rawValue == rhs.rawValue
  }

  case ready

  case executing

  case finishing

  case finished

  case cancelled
}
