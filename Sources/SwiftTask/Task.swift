import Foundation

open class Task: Operation {

  // use the KVO mechanism to indicate that changes to "state" affect other properties as well
  @objc
  class func keyPathsForValuesAffectingIsReady() -> Set<NSObject> {
    return ["state" as NSObject]
  }
  @objc
  class func keyPathsForValuesAffectingIsExecuting() -> Set<NSObject> {
    return ["state" as NSObject]
  }
  @objc
  class func keyPathsForValuesAffectingIsFinished() -> Set<NSObject> {
    return ["state" as NSObject]
  }
  @objc
  class func keyPathsForValuesAffectingIsCancelled() -> Set<NSObject> {
    return ["cancelledState" as NSObject]
  }

  // MARK: - Properties
  private let stateAcces = NSRecursiveLock()
  private let stateQueue = DispatchQueue(label: "Task.state")
  private var _state: State = .ready

  fileprivate var state: State {
    set {
      willChangeValue(forKey: "state")
      stateQueue.sync {
        guard _state != .finished else {
          return }

        self._state = newValue
      }
      didChangeValue(forKey: "state")
    }
    get {
      return stateQueue.sync { _state }
    }
  }
  private(set) var error: TaskError?
  open override var isExecuting: Bool { return state == .executing }
  open override var isFinished: Bool { return state == .finished }

  // MARK: - Cancelled State
  private var _aborted = false
  private var aborted: Bool {
    get {
      return stateQueue.sync { _aborted }
    }
    set {
      stateAcces.lock()
      defer { stateAcces.unlock() }
      guard aborted != newValue else {
        return }

      willChangeValue(forKey: "cancelledState")
      stateQueue.sync { _aborted = newValue }
      didChangeValue(forKey: "cancelledState")

      if newValue {
        observers.forEach { $0.taskDidCancel(self, error: error) }
      }
    }
  }
  open override var isCancelled: Bool {
    return aborted
  }

  // MARK: - Methods
  open override func start() {
    // Cancel task if any of the dependecies were cancelled.
    let op = dependencies.first { $0.isCancelled == true }
    if let task = op as? Task {
      cancel(with: task.error)
    } else if op != nil {
      cancel()
    }

    stateAcces.lock()

    if !isCancelled {
      state = .executing
      observers.forEach { $0.taskDidStart(self) }
      log.debug("Task: \(self) started.")
      execute()
      stateAcces.unlock()
    } else {
      finish()
      stateAcces.unlock()
    }
  }
  /// must have be finished in the execute() override
  open func execute() {
    fatalError("Put all the work here and finish the operation.")
  }

  // MARK: - Observers
  private var observers: [TaskObserver] = []

  public func add(_ observer: TaskObserver) {
    assert(state < .executing)
    observers.append(observer)
  }

  // MARK: - Finishing and Canceling
  open override func cancel() {
    stateAcces.lock()
    defer { stateAcces.unlock() }
    guard !isFinished else {
      return }

    aborted = true

    if state == .executing {
      finish()
    }
  }
  public func cancel(with error: TaskError?) {
    guard !isCancelled && !isFinished else {
      return }
    self.error = error ?? TaskError.cancelError
    cancel()
  }
  public func finish(with error: TaskError?) {
    self.error = error
    finish()
  }

  private var hasAlreadyFinished = false

  public final func finish() {
    stateAcces.lock()
    defer { stateAcces.unlock() }

    guard !hasAlreadyFinished else {
      return }

    state = .finishing

    observers.forEach { $0.taskDidFinish(self, error: error) }
    hasAlreadyFinished = true

    state = .finished
    log.debug("Task: \(self) finished.")
  }

  open override func waitUntilFinished() {
    fatalError("Waiting on operations is an anti-pattern.")
  }

  // MARK: - Custom Operators
  @discardableResult
  public static func ->> (lhs: Task, rhs: Task) -> Task {
    lhs.addDependency(rhs)
    return rhs
  }
}

infix operator ->>: AdditionPrecedence
