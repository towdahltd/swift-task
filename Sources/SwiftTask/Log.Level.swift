import Foundation

extension Log {
  public enum Level: Int, Comparable {
    case verbose, debug, info, warning, error

    var color: String {
      switch self {
      case .verbose:
        return "💜"
      case .debug:
        return "💚"
      case .info:
        return "💙"
      case .warning:
        return "🧡"
      case .error:
        return "❤️"
      }
    }

    // MARK: - Comparable
    public static func < (lhs: Log.Level, rhs: Log.Level) -> Bool {
      return lhs.rawValue < rhs.rawValue
    }
  }
}
