import Foundation

public struct TaskError: Error, LocalizedError, Equatable {
  public private(set) var messages: [String] = []
  public private(set) var code: Int = 0

  public init(_ msg: String, code: Int = 0) {
    messages.append(msg)
    self.code = code
  }

  public init(_ msgs: [String], code: Int = 0) {
    messages = msgs
    self.code = code
  }

  public init(_ taskError: TaskError, and msg: String, code: Int = 0) {
    messages = taskError.messages
    messages.append(msg)
    self.code = code
  }

  public func add(_ msg: String, code: Int = 0) -> TaskError {
    var messages = self.messages
    messages.append(msg)
    return TaskError(messages, code: code)
  }

  public func add(_ msgs: [String], code: Int = 0) -> TaskError {
    return TaskError(messages + msgs, code: code)
  }

  public var errorDescription: String? {
    return messages.first
  }
}

extension TaskError {
  public static var cancelError: TaskError {
    return TaskError("Task has been cancelled.")
  }
}
