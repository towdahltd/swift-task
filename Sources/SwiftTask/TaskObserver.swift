import Foundation

public protocol TaskObserver {
  func taskDidStart(_ task: Task)
  func taskDidCancel(_ task: Task, error: TaskError?)
  func taskDidFinish(_ task: Task, error: TaskError?)
}
