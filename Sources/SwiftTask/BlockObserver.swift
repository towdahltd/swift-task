import Foundation

public final class BlockObserver: TaskObserver {
  // MARK: - Properties
  private let startClosure: ((Task) -> Void)?
  private let cancelClosure: ((Task, TaskError?) -> Void)?
  private let finishClosure: ((Task, TaskError?) -> Void)?

  // MARK: - Initialization
  init(
    start: ((Task) -> Void)? = nil,
    cancel: ((Task, TaskError?) -> Void)? = nil,
    finish: ((Task, TaskError?) -> Void)? = nil
    ) {
    self.startClosure = start
    self.cancelClosure = cancel
    self.finishClosure = finish
  }

  // MARK: - TaskObserver
  public func taskDidStart(_ task: Task) {
    startClosure?(task)
  }
  public func taskDidCancel(_ task: Task, error: TaskError?) {
    cancelClosure?(task, error)
  }
  public func taskDidFinish(_ task: Task, error: TaskError?) {
    finishClosure?(task, error)
  }
}
