import Foundation

public class TaskLogger {
  public static var instance: Log = {
    let log = Log(module: "TaskKit")

    log.minLevel = .verbose
    log.format = """
    $T $M $F.$f:$l ->> $C $m
    """
    return log
  }()
}

let log = TaskLogger.instance
