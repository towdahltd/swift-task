import Foundation

open class BlockTask: Task {
  fileprivate var blocks = [() -> Void]()

  public init(block: @escaping () -> Void) {
    blocks.append(block)
    super.init()
  }

  public convenience init(mainQueue block: @escaping () -> Void) {
    self.init {
      DispatchQueue.main.async { block() }
    }
  }

  open override func execute() {
    blocks.forEach { $0() }
    finish()
  }
}
